import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { Checkbox } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    position: "relative",
    overflow: "auto",
    maxHeight: "80vh",
  },
  listSection: {
    backgroundColor: "inherit",
  },
  ul: {
    backgroundColor: "inherit",
    padding: 0,
  },
}));

export default function PinnedSubheaderList(props) {
  const classes = useStyles();

  const handleToggle = (value) => () => {
    console.log();

    var food = {
      food_detail: value,
      quantity: 1,
    };
    props.callback(food, value);
  };

  //Tăng giảm số lượng
  const handleChange = (e, item) => {
    if (e.target.value < 1) {
      e.target.value = 1;
    }
    item.quantity = Number(e.target.value);
  };

  return (
    <List className={classes.root} subheader={<li />}>
      <li className={classes.listSection}>
        <ul className={classes.ul}>
          {props.checkbox === 0 &&
            props.data.map((item) => (
              <ListItem
                key={item.id}
                style={{ borderBottom: "1px solid black" }}
              >
                <ListItemText
                  primary={item.name}
                  style={{
                    textOverflow: "clip",
                    overflow: "hidden",
                    whiteSpace: "nowrap",
                  }}
                />
                <ListItemText primary={`$${item.price}`} />
                <Checkbox
                  edge="start"
                  checked={props.listCheck.indexOf(item) !== -1}
                  onChange={handleToggle(item)}
                />
              </ListItem>
            ))}
          {props.checkbox === 1 &&
            props.data.map((item) => (
              <ListItem
                key={item.food_detail.id}
                style={{ borderBottom: "1px solid black" }}
              >
                <ListItemText
                  primary={item.food_detail.name}
                  style={{
                    textOverflow: "clip",
                    overflow: "hidden",
                    whiteSpace: "nowrap",
                  }}
                />
                <ListItemText primary={`$${item.food_detail.price}`} />
                {props.quantity === true && (
                  <TextField
                    id={item.food_detail.id}
                    label="Số lượng"
                    type="number"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    defaultValue={1}
                    variant="outlined"
                    style={{ width: "20%" }}
                    onChange={(e) => handleChange(e, item)}
                  />
                )}
                {!props.quantity && <ListItemText primary={item.quantity} />}
              </ListItem>
            ))}
        </ul>
      </li>
    </List>
  );
}
