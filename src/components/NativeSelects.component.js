import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import NativeSelect from "@material-ui/core/NativeSelect";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: 200,
  },
}));

export default function NativeSelects(props) {
  const classes = useStyles();

  const handleChange = (event) => {
    props.callback(event);
  };

  return (
    <div>
      <Grid item xs={12}>
        <FormControl variant="filled" className={classes.formControl} style={{marginLeft: '1%', marginTop: '5px'}}>
          <InputLabel htmlFor="filled-age-native-simple">Table</InputLabel>
          <Select
            native
            onChange={handleChange}
            inputProps={{
              name: "table",
              id: "filled-age-native-simple",
            }}
            
          >
            <option aria-label="None" value="" />
            <option value={1}>Bàn 1</option>
            <option value={2}>Bàn 2</option>
            <option value={3}>Bàn 3</option>
          </Select>
        </FormControl>
      </Grid>
    </div>
  );
}
