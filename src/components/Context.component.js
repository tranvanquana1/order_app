import React, { useState, createContext } from "react";

export const FoodContext = createContext({});

const FoodProvider = (props) => {
  const [item, setItem] = useState([
    {
      id: "1",
      name: "Mỳ tôm",
      price: 4000,
    },
    {
      id: "2",
      name: "Bún cá",
      price: 30000,
    },
    {
      id: "3",
      name: "Beefsteak",
      price: 100000,
    },
    {
      id: "4",
      name: "Burger",
      price: 60000,
    },
    {
      id: "5",
      name: "Bia",
      price: 15000,
    },
    {
      id: "6",
      name: "Rượu táo mèo",
      price: 50000,
    },
    {
      id: "7",
      name: "Coca",
      price: 18000,
    },
  ]);

  const [order, setOrder] = useState([]);
  const [checked, setChecked] = useState([]);
  const [bill, setBill] = useState({
    table: null,
    food: [],
    total: null,
  });

  return (
    <FoodContext.Provider
      value={{
        item,
        setItem,
        order,
        setOrder,
        bill,
        setBill,
        checked,
        setChecked,
      }}
    >
      {props.children}
    </FoodContext.Provider>
  );
};

export default FoodProvider;
